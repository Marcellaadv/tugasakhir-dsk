#include <stdio.h>

int main(){
int s = 10;
int d;

asm ("mov %1, %0\n\t" //d=s
    "inc %0"
    : "=r" (d)
    : "r" (s)
    );

    printf("d++, %d\n", d);

asm ("mov %1, %0\n\t" //d=s
    "dec %0"
    : "=r" (d)
    : "r" (s));

    printf("d--, %d\n", d);

asm ("mov %1, %0\n\t" //d=s
    "add $3, %0"
    : "=r" (d)
    : "r" (s)
    );

    printf("d+=3, %d\n", d);
    
asm ("mov %1, %0\n\t" //d=s
    "sub $3, %0"
    : "=r" (d)
    : "r" (s)
    );

    printf("d-=3, %d\n", d);

asm ("mov %1, %0\n\t" //d=s
    "shl $1, %0"
    : "=r" (d)
    : "r" (s));

    printf("d <<= 1, %d\n", d);

asm ("mov %1, %0\n\t" //d=s
    "shr $1, %0"
    : "=r" (d)
    : "r" (s));

    printf("d >>= 1, %d\n", d);
    return 0;
}
